//TODO sort data based on numbers
//TODO plot the graph

require("dotenv").config();
const cheerio = require("cheerio");
const fetch = require("node-fetch");
const table = require("table").table;
const fs = require("fs");
const url = "https://en.wikipedia.org/wiki/List_of_largest_cities";

let cities;
let tableData = [];
// get a list of major cities
fetch(url)
  .then(res => res.text())
  .then(async body => {
    const $ = cheerio.load(body);
    cities = $(".sortable > tbody > tr > th")
      .slice(9)
      .text()
      .split("\n");
    //for each city, hit the air polution api using the api key
    for (const city of cities) {
      await fetch(
        `http://api.waqi.info/feed/${city}/?token=${process.env.AIRQKEY}`
      )
        .then(res => res.json())
        .then(json => {
          if (json.status === "ok" && json.data.aqi !== "-") {
            console.log("crawling city: ", city);
            console.log("aqi : ", json.data.aqi);
            let tableRow = [];
            tableRow.push(city, json.data.aqi);
            tableData.push(tableRow);
            Promise.resolve("done");
          }
        })
        .catch(err => Promise.reject(err));
    }
    //sort data
    tableData.sort((a, b) => a[1] - b[1]);
    let output = table(tableData);
    // save result to file
    fs.writeFile("aqi.txt", output, "utf8", err => {
      if (err) {
        return console.log(err);
      }
    });
  });
